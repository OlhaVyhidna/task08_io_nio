package vyhidna.displaydirectory;

import java.io.File;
import java.util.Arrays;

public class DisplayComponentsOfDirectory {

  public void displayComponentsOfDirectory() {
    File file = new File("/home/olga/EPAM");
    if (file.exists()) {
      if (file.isDirectory()) {
        printFolderComponents(file);
      } else {
        printFile(file);
      }
    } else {
      System.out.println("File does not exist");
    }
  }

  private void printFolderComponents(File file) {
    File[] files = file.listFiles();
    System.out.println("Folder name " + file.getName() + ", number of components " + files.length);
    if (files.length > 0) {
      Arrays.stream(files).forEach(f -> {
        if (f.isDirectory()) {
          printFolderComponents(f);
        } else {
          printFile(f);
        }
      });
    }
  }

  private void printFile(File file) {
    System.out.println("File name " + file.getName() + ", file size " + file.getUsableSpace());
  }
}
