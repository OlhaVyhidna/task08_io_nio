package vyhidna.somebuffer;

import java.io.IOException;

public class Main {

  public static void main(String[] args) throws IOException {
    SomeBuffer someBuffer = new SomeBuffer();
    someBuffer.readFromChannel();
    someBuffer.writeToChannel();
  }

}
