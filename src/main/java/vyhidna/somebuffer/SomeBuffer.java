package vyhidna.somebuffer;

import java.awt.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

public class SomeBuffer {
//  private final String FILE_TO_READ = "texet.txt";
  private final String FILE_TO_READ = "comparing.txt";
  private final String FILE_TO_WRITE = "write2.txt";
  private StringBuilder text = new StringBuilder();


  public void readFromChannel() throws IOException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(FILE_TO_READ, "r");
    FileChannel channel = randomAccessFile.getChannel();
    final ByteBuffer buffer = ByteBuffer.allocate(1024);
    while (channel.read(buffer)>0){
      buffer.flip();
      String s = new String(buffer.array(), StandardCharsets.UTF_8).trim();
      text.append(s);
      buffer.clear();
    }
    channel.close();
    randomAccessFile.close();

  }

  public void writeToChannel() throws IOException {
    ByteBuffer buffer = ByteBuffer.wrap(text.toString().getBytes());
    Set options = new HashSet<>();
    options.add(StandardOpenOption.CREATE);
    options.add(StandardOpenOption.APPEND);
    Path path = Paths.get(FILE_TO_WRITE);

    FileChannel fileChannel = FileChannel.open(path, options);
    fileChannel.write(buffer);
    fileChannel.close();
  }

}
