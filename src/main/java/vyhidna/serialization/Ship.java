package vyhidna.serialization;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable {
  private int length;
  private String shipName;
  private List<Droid> droids;
  private transient int shipAge;

  public Ship(int length, String shipName, List<Droid> droids, int shipAge) {
    this.length = length;
    this.shipName = shipName;
    this.droids = droids;
    this.shipAge = shipAge;
  }

  @Override
  public String toString() {
    return "Ship{" +
        "length=" + length +
        ", shipName='" + shipName + '\'' +
        ", droids=" + droids +
        ", shipAge=" + shipAge +
        '}';
  }
}
