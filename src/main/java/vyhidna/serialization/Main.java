package vyhidna.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args)  {

    Droid droid =
        new Droid(23, 345, 678, "China", 2020);
    List<Droid> droids = new ArrayList<>();
    droids.add(droid);
    Ship victory = new Ship(67, "Victory", droids, 6);

    ObjectOutputStream objectOutputStream = null;
    try {
      objectOutputStream = new ObjectOutputStream(new FileOutputStream("ship.txt"));
      objectOutputStream.writeObject(victory);
    } catch (IOException e) {
      e.printStackTrace();
    }finally {
      try {
        assert objectOutputStream != null;
        objectOutputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    Ship ship = null;
    ObjectInputStream objectInputStream = null;
    try {
      objectInputStream = new ObjectInputStream(new FileInputStream("ship.txt"));
      ship = (Ship) objectInputStream.readObject();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      try {
        assert objectInputStream != null;
        objectInputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    System.out.println(ship);
  }

}
