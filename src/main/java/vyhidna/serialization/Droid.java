package vyhidna.serialization;

import java.io.Serializable;
//Droid class
public class Droid implements Serializable {
  private int power;
  private long serialNumber;
  //transient field
  private transient int  weight;
  private String producingCountry;
  private int yearOfProduction;

  public Droid(int power, long serialNumber, int weight, String producingCountry,
      int yearOfProduction) {
    this.power = power;
    this.serialNumber = serialNumber;
    this.weight = weight;
    this.producingCountry = producingCountry;
    this.yearOfProduction = yearOfProduction;
  }

  /*
  documentation type comment
  for reading
  in ReadSourceFile class
   */

  @Override
  public String toString() {
    //one more comment
    return "Droid{" +
        "power=" + power +
        ", serialNumber=" + serialNumber +
        ", weight=" + weight +
        ", producingCountry='" + producingCountry + '\'' +
        ", yearOfProduction=" + yearOfProduction +
        '}';
  }
}
