package vyhidna.sourcecode;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ReadSourceCodeFile {
  private String filePath;
  private List<String> comments;

  public ReadSourceCodeFile() {
    this("/home/olga/EPAM/ofline/FirstLesson/vyhidna_IO_NIO/src/main/java/vyhidna/serialization/Droid.java");
  }

  public ReadSourceCodeFile(String filePath) {
    this.filePath = filePath;
    comments = new ArrayList<>();
  }


  public List<String> readCommentsFromFile(){
    try (final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)))){

      String line = bufferedReader.readLine();
      while (line!=null){
        if (line.contains("//")){
          comments.add(line.trim());
          line = bufferedReader.readLine();
        }else if (line.contains("/*")||line.contains("/**")){
          StringBuilder documentComment = new StringBuilder();
          boolean commentEnded = false;
          while (!commentEnded){
            documentComment.append(line.trim() + "\n");
            if (line.contains("*/")){
              commentEnded = true;
            }
            line = bufferedReader.readLine();
          }
          comments.add(documentComment.toString());
        }
        line = bufferedReader.readLine();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return comments;
  }


}
