package vyhidna.sourcecode;

import java.util.List;

public class Main {

  public static void main(String[] args) {
    ReadSourceCodeFile readSourceCodeFile = new ReadSourceCodeFile();
    List<String> list = readSourceCodeFile.readCommentsFromFile();
    for (String comment : list) {
      System.out.println(comment);
    }
  }

}
