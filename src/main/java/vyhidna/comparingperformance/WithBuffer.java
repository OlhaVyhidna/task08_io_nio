package vyhidna.comparingperformance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class WithBuffer {
  private final String FILE_TO_READ = "texet.txt";
  private final String FILE_TO_WRITE = "write.txt";

  List<String> lines;

  public WithBuffer() {
    this.lines = new ArrayList<>();
  }

  public void readWithBuffer(){
    this.lines = new ArrayList<>();
    try(final BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(new FileInputStream(FILE_TO_READ)))) {
      String s = bufferedReader.readLine();
      while (s!=null){
        lines.add(s.trim());
        System.out.println(s);
        s = bufferedReader.readLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Ended reading");
  }

  public void readWith1MBBuffer(){
    final long start = System.nanoTime();
    try(final BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(new FileInputStream(FILE_TO_READ)), (1*1024*1024))) {
      String s = bufferedReader.readLine();
      while (s!=null){
        s = bufferedReader.readLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    final long end = System.nanoTime();
    long elapsedTime = end - start;
    double seconds = (double)elapsedTime / 1_000_000_000.0;
    System.out.println("Ended reading");
    System.out.println("Read with buffer 1 MG in " + seconds + "seconds" );
  }

  public void readWith10MBBuffer(){
    final long start = System.nanoTime();
    try(final BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(new FileInputStream(FILE_TO_READ)), (10*1024*1024))) {
      String s = bufferedReader.readLine();
      while (s!=null){
        s = bufferedReader.readLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    final long end = System.nanoTime();
    long elapsedTime = end - start;
    double seconds = (double)elapsedTime / 1_000_000_000.0;
    System.out.println("Ended reading");
    System.out.println("Read with buffer 10 MG in " + seconds + "seconds" );
  }

  public void readWith30MBBuffer(){
    final long start = System.nanoTime();
    try(final BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(new FileInputStream(FILE_TO_READ)), (30*1024*1024))) {
      String s = bufferedReader.readLine();
      while (s!=null){
        s = bufferedReader.readLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    final long end = System.nanoTime();
    long elapsedTime = end - start;
    double seconds = (double)elapsedTime / 1_000_000_000.0;
    System.out.println("Ended reading");
    System.out.println("Read with buffer 30 MG in " + seconds + "seconds" );
  }

  public void writeWithBuffer(){
    readWithBuffer();
    try(final BufferedWriter bufferedWriter =
        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FILE_TO_WRITE)))) {
      for (String line : lines) {
        bufferedWriter.write(line);
        bufferedWriter.newLine();
      }

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Ended writing");
  }
}
