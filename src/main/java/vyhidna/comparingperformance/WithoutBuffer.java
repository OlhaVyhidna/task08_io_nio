package vyhidna.comparingperformance;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class WithoutBuffer {
private List<Integer> lines;
private final String FILE_TO_READ = "texet.txt";
private final String FILE_TO_WRITE = "write.txt";

  public WithoutBuffer() {
    readWithoutBuffer();
  }

  public void readWithoutBuffer(){
  this.lines = new ArrayList<>();
  try(final InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(FILE_TO_READ))) {
    int read = inputStreamReader.read();
    while (read!=-1){
      System.out.println(read);
      lines.add(read);
      read = inputStreamReader.read();
    }
  } catch (IOException e) {
    e.printStackTrace();
  }
    System.out.println("Ended reading");
}

public void writeWithoutBuffer(){
      try(final BufferedWriter bufferedWriter =
        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FILE_TO_WRITE)))) {
      for (int line : lines) {
        bufferedWriter.write(line);
      }

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  System.out.println("Ended writing");
}


}
