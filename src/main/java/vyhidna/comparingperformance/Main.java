package vyhidna.comparingperformance;

public class Main {


  public static void main(String[] args) {
    WithoutBuffer withoutBuffer = new WithoutBuffer();
    WithBuffer withBuffer = new WithBuffer();
    withBuffer.readWithBuffer();
    withBuffer.writeWithBuffer();
    withBuffer.readWith1MBBuffer();
    withBuffer.readWith10MBBuffer();
    withBuffer.readWith30MBBuffer();

    withoutBuffer.readWithoutBuffer();
    withoutBuffer.writeWithoutBuffer();
  }


}
